import logging

import messages
from irc import irc_manager


MSG_LOG_FILE = 'messages.log'

msg_logger = logging.getLogger(__name__)
msg_logger.setLevel(logging.INFO)
msg_handler = logging.FileHandler(MSG_LOG_FILE)
msg_handler.setFormatter(logging.Formatter("%(asctime)s\n%(message)s\n"))
msg_logger.addHandler(msg_handler)


class GitLabEvent:
    def __init__(self, config_section, event_type, data):
        self.config_section = config_section
        self.event_type = event_type
        self.data = data
        self.hash_length = self.config_section.getint('hash_length', fallback=7)

    def _short_hash(self, h):
        return h[:self.hash_length]

    def _pass_event(self, event_name):
        return not self.config_section.getboolean(
            event_name,
            fallback=True
        )

    def send_msg(self, msg):
        irc_manager.send_msg(self.config_section, msg)
        msg_logger.info(msg)

    def handle(self):
        if self.event_type == "Push Hook":
            error = self.handle_push_event()
        elif self.event_type == "Tag Push Hook":
            error = self.handle_tag_push_event()
        elif self.event_type == "Issue Hook":
            error = self.handle_issue_event()
        elif self.event_type == "Note Hook":
            error = self.handle_comment_event()
        elif self.event_type == "Merge Request Hook":
            error = self.handle_merge_request_event()
        elif self.event_type == "Wiki Page Hook":
            error = self.handle_wiki_page_event()
        elif self.event_type == "Pipeline Hook":
            error = self.handle_pipeline_event()
        elif self.event_type == "Job Hook":
            error = self.handle_job_event()
        else:
            error = "Unsupported event type '{}'".format(self.event_type)

        return error
        

    def handle_push_event(self):
        if self.data['after'] == "0"*40:
            return self.handle_delete_branch_event()
        if self.data['before'] == "0"*40:
            return self.handle_create_branch_event()

        if self._pass_event('event_push'):
            return None

        compare_url = messages.push_url.format(
            root=self.data['project']['web_url'],
            before=self._short_hash(self.data['before']),
            after=self._short_hash(self.data['after'])
        )
    
        msg = messages.push.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user_name'],
            nb_commits=self.data['total_commits_count'],
            branch=self.data['ref'][len('refs/heads/'):],
            compare_url=compare_url
        )
    
        self.send_msg(msg)
        return None


    def handle_create_branch_event(self):
        if self._pass_event('event_branch_create'):
            return None

        branch_url = messages.branch_url.format(
            root=self.data['project']['web_url'],
            branch=self.data['ref'][len('refs/heads/'):]
        )

        msg = messages.branch_create.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user_name'],
            branch=self.data['ref'][len('refs/heads/'):],
            branch_url=branch_url
        )

        self.send_msg(msg)
        return None


    def handle_delete_branch_event(self):
        if self._pass_event('event_branch_delete'):
            return None

        msg = messages.branch_delete.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user_name'],
            branch=self.data['ref'][len('refs/heads/'):],
        )

        self.send_msg(msg)
        return None
    
    
    def handle_tag_push_event(self):
        if self._pass_event('event_tag'):
            return None

        tag_url = messages.tag_push_url.format(
            root=self.data['project']['web_url'],
            tag=self.data['ref'][len('refs/tags/'):]
        )
    
        msg = messages.tag_push.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user_name'],
            tag=self.data['ref'][len('refs/tags/'):],
            tag_url=tag_url
        )
    
        self.send_msg(msg)
        return None
    
    
    def handle_issue_event(self):
        has_labels = bool(self.data['labels'])
        action = self.data['object_attributes']['action']
        if action == "open":
            if self._pass_event('event_issue_open'):
                return None
            if has_labels:
                message = messages.issue_open_labels
            else:
                message = messages.issue_open_no_label
        elif action == "update":
            if self._pass_event('event_issue_update'):
                return None
            if has_labels:
                message = messages.issue_update_labels
            else:
                message = messages.issue_update_no_label
        elif action == "close":
            if self._pass_event('event_issue_close'):
                return None
            if has_labels:
                message = messages.issue_close_labels
            else:
                message = messages.issue_close_no_label
        else:
            return "Unknown action '{}'".format(action)
    
        labels = ", ".join(label['title'] for label in self.data['labels'])
    
        msg = message.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user']['name'],
            issue_id=self.data['object_attributes']['iid'],
            issue_name=self.data['object_attributes']['title'],
            labels=labels,
            issue_url=self.data['object_attributes']['url']
        )
    
        self.send_msg(msg)
        return None
    
    
    def handle_comment_event(self):
        comment_type = self.data['object_attributes']['noteable_type']
        if comment_type == "Commit":
            return self.handle_comment_commit_event()
        elif comment_type == "MergeRequest":
            return self.handle_comment_merge_request_event()
        elif comment_type == "Issue":
            return self.handle_comment_issue_event()
        elif comment_type == "Snippet":
            return self.handle_comment_snippet_event()
        else:
            return "Unknown comment type '{}'".format(comment_type)
    
    
    def handle_comment_commit_event(self):
        if self._pass_event('event_comment_commit'):
            return None

        msg = messages.comment_commit.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user']['name'],
            commit_sha=self._short_hash(self.data['commit']['id']),
            commit_title=self.data['commit']['title'],
            commit_author=self.data['commit']['author']['name'],
            commit_url=self.data['object_attributes']['url']
        )
    
        self.send_msg(msg)
        return None
    
    
    def handle_comment_merge_request_event(self):
        if self._pass_event('event_comment_mr'):
            return None

        msg = messages.comment_mr.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user']['name'],
            mr_id=self.data['merge_request']['iid'],
            mr_title=self.data['merge_request']['title'],
            comment_url=self.data['object_attributes']['url']
        )
    
        self.send_msg(msg)
        return None
    
    
    def handle_comment_issue_event(self):
        if self._pass_event('event_comment_issue'):
            return None

        msg = messages.comment_issue.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user']['name'],
            issue_id=self.data['issue']['iid'],
            issue_title=self.data['issue']['title'],
            comment_url=self.data['object_attributes']['url']
        )
    
        self.send_msg(msg)
        return None
    
    
    def handle_comment_snippet_event(self):
        if self._pass_event('event_comment_snippet'):
            return None

        msg = messages.comment_snippet.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user']['name'],
            snippet_id=self.data['snippet']['id'],
            snippet_title=self.data['snippet']['title'],
            comment_url=self.data['object_attributes']['url']
        )
    
        self.send_msg(msg)
        return None
    
    
    def handle_merge_request_event(self):
        has_labels = bool(self.data['labels'])
        action = self.data['object_attributes']['action']
        if action == "open":
            if self._pass_event('event_mr_open'):
                return None
            if has_labels:
                message = messages.merge_request_open_labels
            else:
                message = messages.merge_request_open_no_label
        elif action == "update":
            if self._pass_event('event_mr_update'):
                return None
            if has_labels:
                message = messages.merge_request_update_labels
            else:
                message = messages.merge_request_update_no_label
        elif action == "merge":
            if self._pass_event('event_mr_merge'):
                return None
            if has_labels:
                message = messages.merge_request_merge_labels
            else:
                message = messages.merge_request_merge_no_label
        elif action == "close":
            if self._pass_event('event_mr_close'):
                return None
            if has_labels:
                message = messages.merge_request_close_labels
            else:
                message = messages.merge_request_close_no_label
        else:
            return "Unknown action '{}'".format(action)
    
        labels = ", ".join(label['title'] for label in self.data['labels'])
    
        msg = message.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user']['name'],
            mr_id=self.data['object_attributes']['iid'],
            mr_title=self.data['object_attributes']['title'],
            labels=labels,
            mr_url=self.data['object_attributes']['url']
        )
    
        self.send_msg(msg)
        return None
        
    
    def handle_wiki_page_event(self):
        action = self.data['object_attributes']['action']
        if action == "create":
            if self._pass_event('event_wiki_create'):
                return None
            message = messages.wiki_page_create
        elif action == "update":
            if self._pass_event('event_wiki_update'):
                return None
            message = messages.wiki_page_update
        elif action == "delete":
            if self._pass_event('event_wiki_delete'):
                return None
            message = messages.wiki_page_delete
        else:
            return "Unknown action '{}'".format(action)
    
        msg = message.format(
            project_name=self.data['project']['name'],
            user_name=self.data['user']['name'],
            page_title=self.data['object_attributes']['title'],
            page_url=self.data['object_attributes']['url']
        )
    
        self.send_msg(msg)
        return None
    
    
    def handle_pipeline_event(self):
        status = self.data['object_attributes']['status']
        if status == "created":
            if self._pass_event('event_pipeline_create'):
                return None
            message = messages.pipeline_created
        elif status == "pending":
            if self._pass_event('event_pipeline_pending'):
                return None
            message = messages.pipeline_pending
        elif status == "running":
            if self._pass_event('event_pipeline_run'):
                return None
            message = messages.pipeline_running
        elif status == "success":
            if self._pass_event('event_pipeline_succeed'):
                return None
            message = messages.pipeline_success
        elif status == "failed":
            if self._pass_event('event_pipeline_fail'):
                return None
            message = messages.pipeline_failed
        else:
            return "Unknown status '{}'".format(status)
    
        pipeline_url = messages.pipeline_url.format(
            root=self.data['project']['web_url'],
            pipeline=self.data['object_attributes']['id']
        )
    
        msg = message.format(
            project_name=self.data['project']['name'],
            pipeline_id=self.data['object_attributes']['id'],
            commit_sha=self._short_hash(self.data['commit']['id']),
            commit_title=self.data['commit']['message'].split('\n', maxsplit=1)[0],
            commit_author=self.data['commit']['author']['name'],
            branch=self.data['object_attributes']['ref'],
            duration=self.data['object_attributes']['duration'],
            pipeline_url=pipeline_url
        )
    
        self.send_msg(msg)
        return None
    
    
    def handle_job_event(self):
        status = self.data['build_status']
        if status == "created":
            if self._pass_event('event_job_create'):
                return None
            message = messages.job_created
        elif status == "running":
            if self._pass_event('event_job_run'):
                return None
            message = messages.job_running
        elif status == "success":
            if self._pass_event('event_job_succeed'):
                return None
            message = messages.job_success
        elif status == "failed":
            if self._pass_event('event_job_fail'):
                return None
            message = messages.job_failed
        else:
            return "Unknown status '{}'".format(status)
    
        job_url = messages.job_url.format(
            root=self.data['repository']['homepage'],
            job=self.data['build_id']
        )
    
        msg = message.format(
            project_name=self.data['repository']['name'],
            job_id=self.data['build_id'],
            commit_sha=self._short_hash(self.data['commit']['sha']),
            commit_title=self.data['commit']['message'].split('\n', maxsplit=1)[0],
            commit_author=self.data['commit']['author_name'],
            branch=self.data['ref'],
            duration=self.data['build_duration'],
            job_url=job_url
        )
    
        self.send_msg(msg)
        return None
    
