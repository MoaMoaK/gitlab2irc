from queue import Queue
from threading import Thread

from simple_pyirc import IRCClient


class IRCManager:
    def __init__(self):
        self.irc_clients = {}
        self.msg_queue = Queue()
        self.process_thread = Thread(target=self._process_msg_queue)
        self.process_thread.start()

    def _get_irc_client(self, network, nickname, port, password):
        if network not in self.irc_clients:
            self.irc_clients[network] = {}
        if nickname not in self.irc_clients[network]:
            self.irc_clients[network][nickname] = IRCClient(network=network,
                                                            nickname=nickname,
                                                            port=port,
                                                            password=password)
        return self.irc_clients[network][nickname]

    def send_msg(self, config_section, msg):
        recipient = {
            'network': config_section.get('network'),
            'port': config_section.getint('port', fallback=6667),
            'nickname': config_section.get('nickname'),
            'password': config_section.get('password', fallback=None),
            'chan': config_section.get('channel')
        }

        self.msg_queue.put((recipient, msg))

    def _post_to_irc(self, recipient, msg):
        irc_client = self._get_irc_client(recipient['network'],
                                          recipient['nickname'],
                                          recipient['port'],
                                          recipient['password'])
        chan = recipient['chan']
        irc_client.join(chan)

        for m in msg.split('\n'):
            irc_client.privmsg(chan, m)

    def _process_msg_queue(self):
        while True:
            item = self.msg_queue.get()
            if item is None:
                continue

            self._post_to_irc(*item)

            self.msg_queue.task_done()


irc_manager = IRCManager()
