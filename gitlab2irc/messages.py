"""Defines the template to use for every event.
"""

# Push Event
push = ("[{project_name}] {user_name} pushed {nb_commits} commit to branch '{branch}'\n"
        "{compare_url}")
push_url = "{root}/compare/{before}...{after}"
branch_create = ("[{project_name}] {user_name} created branch '{branch}'\n"
                 "{branch_url}")
branch_delete = "[{project_name}] {user_name} deleted branch '{branch}'"
branch_url = "{root}/tree/{branch}"



# Tag Push Event
tag_push = ("[{project_name}] {user_name} pushed tag '{tag}'\n"
            "{tag_url}")
tag_push_url = "{root}/tags/{tag}"


# Issue Event
issue_open_labels = ("[{project_name}] {user_name} opened #{issue_id} {issue_name} ({labels})\n"
                     "{issue_url}")
issue_update_labels = ("[{project_name}] {user_name} updated #{issue_id} {issue_name} ({labels})\n"
                       "{issue_url}")
issue_close_labels = ("[{project_name}] {user_name} closed #{issue_id} {issue_name} ({labels})\n"
                      "{issue_url}")
issue_open_no_label = ("[{project_name}] {user_name} opened #{issue_id} {issue_name}\n"
                       "{issue_url}")
issue_update_no_label = ("[{project_name}] {user_name} updated #{issue_id} {issue_name}\n"
                         "{issue_url}")
issue_close_no_label = ("[{project_name}] {user_name} closed #{issue_id} {issue_name}\n"
                        "{issue_url}")


# Comment Event
comment_commit = ("[{project_name}] {user_name} commented on {commit_sha} {commit_title} ({commit_author})\n"
                  "{comment_url}")
comment_mr = ("[{project_name}] {user_name} commented on !{mr_id} {mr_title}\n"
              "{comment_url}")
comment_issue = ("[{project_name}] {user_name} commented on #{issue_id} {issue_title}\n"
                 "{comment_url}")
comment_snippet = ("[{project_name}] {user_name} commented on ${snippet_id} {snippet_title}\n"
                   "{comment_url}")


# Merge Request Event
merge_request_open_labels = ("[{project_name}] {user_name} opened !{mr_id} {mr_title} ({labels})\n"
                             "{mr_url}")
merge_request_update_labels = ("[{project_name}] {user_name} updated !{mr_id} {mr_title} ({labels})\n"
                               "{mr_url}")
merge_request_merge_labels = ("[{project_name}] {user_name} merged !{mr_id} {mr_title} ({labels})\n"
                              "{mr_url}")
merge_request_close_labels = ("[{project_name}] {user_name} closed !{mr_id} {mr_title} ({labels})\n"
                              "{mr_url}")
merge_request_open_no_label = ("[{project_name}] {user_name} opened !{mr_id} {mr_title}\n"
                               "{mr_url}")
merge_request_update_no_label = ("[{project_name}] {user_name} updated !{mr_id} {mr_title}\n"
                                 "{mr_url}")
merge_request_merge_no_label = ("[{project_name}] {user_name} merged !{mr_id} {mr_title}\n"
                                "{mr_url}")
merge_request_close_no_label = ("[{project_name}] {user_name} closed !{mr_id} {mr_title}\n"
                                "{mr_url}")


# Wiki Page Event
wiki_page_create = ("[{project_name}] {user_name} created wiki page {page_title}\n"
                    "{page_url}")
wiki_page_update = ("[{project_name}] {user_name} updated wiki page {page_title}\n"
                    "{page_url}")
wiki_page_delete = ("[{project_name}] {user_name} deleted wiki page {page_title}\n"
                    "{page_url}")


# Pipeline Event
pipeline_created = ("[{project_name}] Pipeline #{pipeline_id} for {commit_sha} {commit_title} ({commit_author}) on branch {branch} was created\n"
                    "{pipeline_url}")
pipeline_pending = ("[{project_name}] Pipeline #{pipeline_id} for {commit_sha} {commit_title} ({commit_author}) on branch {branch} is pending\n"
                    "{pipeline_url}")
pipeline_running = ("[{project_name}] Pipeline #{pipeline_id} for {commit_sha} {commit_title} ({commit_author}) on branch {branch} was started\n"
                    "{pipeline_url}")
pipeline_success = ("[{project_name}] Pipeline #{pipeline_id} for {commit_sha} {commit_title} ({commit_author}) on branch {branch} passed ({duration}s)\n"
                    "{pipeline_url}")
pipeline_failed = ("[{project_name}] Pipeline #{pipeline_id} for {commit_sha} {commit_title} ({commit_author}) on branch {branch} failed ({duration}s)\n"
                   "{pipeline_url}")
pipeline_url = "{root}/pipelines/{pipeline}"


# Job Event
job_created = ("[{project_name}] Job #{job_id} for {commit_sha} {commit_title} ({commit_author}) on branch {branch} was created\n"
               "{job_url}")
job_running = ("[{project_name}] Job #{job_id} for {commit_sha} {commit_title} ({commit_author}) on branch {branch} was started\n"
               "{job_url}")
job_success = ("[{project_name}] Job #{job_id} for {commit_sha} {commit_title} ({commit_author}) on branch {branch} passed ({duration}s)\n"
               "{job_url}")
job_failed = ("[{project_name}] Job #{job_id} for {commit_sha} {commit_title} ({commit_author}) on branch {branch} failed ({duration}s)\n"
              "{job_url}")
job_url = "{root}/-/jobs/{job}"

