from configparser import ConfigParser
import logging

from flask import Flask, request

from simple_pyirc import IRCClient

from gitlab import GitLabEvent, MSG_LOG_FILE


EVENT_TYPE_KEY = 'X-Gitlab-Event'
TOKEN_KEY = 'X-Gitlab-Token'

app = Flask(__name__)

config = ConfigParser()
config.read('config.ini')


@app.route('/', methods=['GET'])
def index():
    with open(MSG_LOG_FILE) as f:
        logs = f.read()

    html = "\n".join("<p>{}</p>".format(msg.replace('\n', "<br \>")) for msg in logs.split('\n\n'))

    return html


@app.route('/webhook', methods=['POST'])
def webhook():
    app.logger.info("Webhook triggered")

    # Check the presence of the token key in headers
    if TOKEN_KEY not in request.headers:
        error = "No '{}' header found".format(TOKEN_KEY)
        app.logger.error(error)
        return error, 400, {}

    # Check the validity of the token
    token = request.headers[TOKEN_KEY]
    if token not in config:
        error = "Invalid token"
        app.logger.error(error)
        return error, 401, {}

    # Check the presence of the event_type key in headers
    if EVENT_TYPE_KEY not in request.headers:
        error = "No '{}' header found".format(EVENT_TYPE_KEY)
        app.logger.error(error)
        return error, 400, {}

    # Get the event_type 
    event_type = request.headers[EVENT_TYPE_KEY]
    app.logger.info("Event type: "+event_type)

    # Loads JSON data from the request
    try:
        data = request.json
    except Exception:
        error = "Missformated JSON body"
        app.logger.error(error)
        return error, 400, {}

    # Get the configuration about this project
    config_section = config[token]

    # Handle the event
    error = GitLabEvent(config_section, event_type, data).handle()

    # Returns 200 if no errors, else 400
    if error is None:
        app.logger.info("Webhook handled with success")
        return 'Ok'
    else:
        app.logger.error(error)
        return error, 400, {}
